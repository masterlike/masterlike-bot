package bot

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// AuthenticationMessage is used for creating a customized button message
func AuthenticationMessage(recipientID string) string {

	buttonYes := Button{
		Type:    "postback",
		Title:   "Muito!",
		Payload: "{\"question\":\"authentication\", \"answer\":\"yes\"}",
	}
	buttonNo := Button{
		Type:    "postback",
		Title:   "Nem ligo...",
		Payload: "{\"question\":\"authentication\", \"answer\":\"no\"}",
	}
	var buttons []Button
	buttons = append(buttons, buttonYes, buttonNo)
	payload := Payload{
		TemplateType: "button",
		Text: "Agradecemos o seu patrocínio!" +
			" Imagina que agora você pudesse ajudar suas páginas favoritas a gerar conteúdo de seu interesse!" +
			" O quanto isso é interessante para você?",
		Buttons: buttons,
	}
	attachment := Attachment{
		Type:    "template",
		Payload: payload,
	}

	message := Message{
		Attachment: attachment,
	}
	recipient := Recipient{ID: recipientID}

	bm := ButtonMessage{
		Message:   message,
		Recipient: recipient,
	}

	buf, err := json.Marshal(bm)
	if err != nil {
		return err.Error()
	}

	return string(buf)
}

// FirstPositiveAuthenticationMessage is used for creating a customized button message
func FirstPositiveAuthenticationMessage(recipientID string) string {

	buttonYes := Button{
		Type:  "web_url",
		Title: "Virar membro!",
		URL:   "http://masterlike.com.br",
	}
	var buttons []Button
	buttons = append(buttons, buttonYes)
	payload := Payload{
		TemplateType: "button",
		Text:         "Sabia que você é uma pessoa legal! Para se tornar membro exclusivo das suas páginas favoritas é só clicar no link abaixo:",
		Buttons:      buttons,
	}
	attachment := Attachment{
		Type:    "template",
		Payload: payload,
	}

	message := Message{
		Attachment: attachment,
	}
	recipient := Recipient{ID: recipientID}

	bm := ButtonMessage{
		Message:   message,
		Recipient: recipient,
	}

	buf, err := json.Marshal(bm)
	if err != nil {
		return err.Error()
	}

	return string(buf)
}

// FirstNegativeAuthenticationMessage is used for creating a customized button message
func FirstNegativeAuthenticationMessage(recipientID string) string {

	buttonYes := Button{
		Type:    "postback",
		Title:   "Me arrependi!",
		Payload: "{\"question\":\"firstNegative\", \"answer\":\"yes\"}",
	}
	buttonNo := Button{
		Type:    "postback",
		Title:   "Não, obrigado! =D",
		Payload: "{\"question\":\"firstNegative\", \"answer\":\"no\"}",
	}
	var buttons []Button
	buttons = append(buttons, buttonYes, buttonNo)
	payload := Payload{
		TemplateType: "button",
		Text: "Realmente te entendo! Seria bem chato receber conteúdo exclusivo que ninguém mais tem acesso," +
			" concorrer a prêmios e ter a possibilidade de influenciar os posts da suas páginas favoritas.",
		Buttons: buttons,
	}
	attachment := Attachment{
		Type:    "template",
		Payload: payload,
	}

	message := Message{
		Attachment: attachment,
	}
	recipient := Recipient{ID: recipientID}

	bm := ButtonMessage{
		Message:   message,
		Recipient: recipient,
	}

	buf, err := json.Marshal(bm)
	if err != nil {
		return err.Error()
	}

	return string(buf)
}

// ConfirmationMessage is used for creating a customized button message
func ConfirmationMessage(recipientID string) string {

	recipient := fbRecipient{
		ID: recipientID,
	}
	message := fbMessage{
		Text: "Parabéns! Você acaba de virar a pessoa mais legal da conversa! Além de ter acesso a informações exclusivas, você" +
			" participará diretamente da geração de conteúdo das suas páginas favoritas, como também concorrer a brindes!",
	}

	msg := struct {
		Recipient fbRecipient `json:"recipient"`
		Message   fbMessage   `json:"message"`
	}{Recipient: recipient, Message: message}

	buf, err := json.Marshal(msg)
	if err != nil {
		return err.Error()
	}
	return string(buf)
}

// StartTest is used for creating a customized button message
func StartTest(recipientID string) string {

	recipient := fbRecipient{
		ID: recipientID,
	}
	message := fbMessage{
		Text: "Start test",
	}

	msg := struct {
		Recipient fbRecipient `json:"recipient"`
		Message   fbMessage   `json:"message"`
	}{Recipient: recipient, Message: message}

	buf, err := json.Marshal(msg)
	if err != nil {
		return err.Error()
	}
	return string(buf)
}

// SendMessage is for
func SendMessage(recipient string, message string) {

	// msg := mountMessage(recipient)

	URL := "https://graph.facebook.com/v2.6/1703996543196270/messages?access_token=" + "EAAZAKwZAxilUwBAEW9vA4kSTWQhbs7xF7FbjG5OWXFCup2BItCHLKUoEB755JT0Hi6lAL4ZB9TTK4Ke6aOZBOmz0rbWcmRelHKhdIcgEwNJ8yUJPX5cZAIjCEGNBZAzNCoWyZCZALZCF0ZA9gSuN4e82FBOeoSL4z2j1sJZCTpB9wpOHAZDZD"

	req, _ := http.NewRequest("POST",
		URL,
		bytes.NewBuffer([]byte(message)),
	)
	req.Header.Set("Content-Type", "application/json")

	// Execute http POST call
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
		// return
	}
	defer resp.Body.Close()

	// Parse response from body and print.
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
		// return
	}

	fmt.Println("Response body: ", string(body))

	return
}

func checkPayloadAndSendMessage(payload string, recipient string) {

	var pl struct {
		Question string `json:"question"`
		Answer   string `json:"answer"`
	}
	err := json.Unmarshal([]byte(payload), &pl)
	if err != nil {
		panic(err)
		//return
	}
	fmt.Println("Payload received:", payload)

	if pl.Question == "authentication" {
		if pl.Answer == "yes" {
			SendMessage(recipient, ConfirmationMessage(recipient))
		} else {
			SendMessage(recipient, FirstNegativeAuthenticationMessage(recipient))
		}
	} else if pl.Question == "firstNegative" {
		if pl.Answer == "yes" {
			SendMessage(recipient, ConfirmationMessage(recipient))
		}
		// TODO Implement else case if time!
	}

}
