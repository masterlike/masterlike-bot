package bot

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"masterlike-bot/config"
	"net/http"
)

const (
	//PollType indicates as poll the response the messenger needs to process
	PollType = "poll"
)

// Poll comment here TODO
type Poll struct {
	Question Question
	Answers  []Answer
	PageID   string `bson:"page_id"`
}

// Question comment here TODO
type Question struct {
	ID   int    `bson:"question_id"`
	Text string `bson:"question"`
}

// Answer comment here TODO
type Answer struct {
	ID   int    `bson:"answer_id"`
	Text string `json:"text"`
}

// PollPayload comment TODO
type PollPayload struct {
	QuestionID int    `json:"question_id"`
	AnswerID   int    `json:"answer_id"`
	Type       string `json:"type"`
}

type request struct {
	Method      string `json:"method"`
	RelativeURL string `json:"relative_url"`
	Body        string `json:"body"`
}

// Message comment here TODO
func (p *Poll) Message(userID string) (string, error) {

	var answerButtons []Button
	for _, answer := range p.Answers {

		payload, err := getPollPayload(p.Question.ID, answer.ID)
		if err != nil {
			return "", err
		}
		answerButtons = append(answerButtons, Button{
			Type:    "postback",
			Title:   answer.Text,
			Payload: payload,
		})
	}

	payload := Payload{
		TemplateType: "button",
		Text:         p.Question.Text,
		Buttons:      answerButtons,
	}

	bm := ButtonMessage{
		Message: Message{
			Attachment: Attachment{
				Type:    "template",
				Payload: payload,
			},
		},
		Recipient: Recipient{
			ID: userID,
		},
	}

	buf, err := json.Marshal(bm)
	if err != nil {
		return "", err
	}

	return string(buf), nil
}

// BatchMessages comment here TODO
func (p *Poll) BatchMessages(usersIDs []string) string {

	var reqBatch []request
	for _, userID := range usersIDs {
		message, err := p.Message(userID)
		if err != nil {
			continue
		}

		reqBatch = append(reqBatch, request{
			Method:      "POST",
			RelativeURL: "/" + config.BotPageID + "/messages?access_token=" + config.Token,
			Body:        message,
		})
	}

	buf, _ := json.Marshal(reqBatch)

	return string(buf)
}

// SendToAllUsers comment here TODO
func (p *Poll) SendToAllUsers(usersIDs []string) error {
	URL := config.BaseAPI + "/" + config.BotPageID + "/messages?access_token=" + config.Token
	// "&batch=" + p.BatchMessages(usersIDs)
	fmt.Println("Verify URL: ", URL)

	for _, userID := range usersIDs {
		message, _ := p.Message(userID)

		req, _ := http.NewRequest("POST",
			URL,
			bytes.NewBuffer([]byte(message)),
		)
		req.Header.Set("Content-Type", "application/json")

		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			continue
		}
		defer resp.Body.Close()

		// Parse response from body and print.
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			panic(err)
			// return
		}

		fmt.Println("Response body: ", string(body))
	}

	return nil
}

func getPollPayload(questionID, answerID int) (string, error) {

	payload := PollPayload{
		QuestionID: questionID,
		AnswerID:   answerID,
		Type:       PollType,
	}

	buf, err := json.Marshal(payload)
	if err != nil {
		return "", err
	}

	return string(buf), nil
}
