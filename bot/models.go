package bot

type fbSender struct {
	ID string `json:"id"`
}

type fbRecipient struct {
	ID string `json:"id"`
}

type fbMessage struct {
	Text string `json:"text"`
}

type fbMessaging struct {
	Sender    fbSender    `json:"sender"`
	Recipient fbRecipient `json:"recipient"`
	TS        int64       `json:"timestamp"`
	Message   fbMessage   `json:"message,omitempty"`
	Postback  fbPostback  `json:"postback,omitempty"`
	Optin     fbOptin     `json:"optin,omitempty"`
}

type fbOptin struct {
	Ref string `json:"ref,omitempty"`
}

type fbPostback struct {
	Payload string `json:"payload,omitempty"`
}

type fbEntry struct {
	Messaging []fbMessaging `json:"messaging"`
}

type fbMsgEntry struct {
	Entry []fbEntry `json:"entry"`
}

type fbSendMsgEntry struct {
	Recipient fbRecipient `json:"recipient"`
	Message   fbMessage   `json:"message"`
}

// ButtonMessage Format
type ButtonMessage struct {
	Recipient Recipient `json:"recipient"`
	Message   Message   `json:"message"`
}

// Recipient message format
type Recipient struct {
	ID string `json:"id"`
}

// Message Format for Sending Button
type Message struct {
	Attachment Attachment `json:"attachment"`
}

// Attachment for sending button messages
type Attachment struct {
	Type    string  `json:"type"`
	Payload Payload `json:"payload"`
}

// Payload for sending button messages
type Payload struct {
	TemplateType string   `json:"template_type"`
	Text         string   `json:"text"`
	Buttons      []Button `json:"buttons"`
}

// Button for sending button messages
type Button struct {
	Type    string `json:"type"`
	URL     string `json:"url,omitempty"`
	Title   string `json:"title"`
	Payload string `json:"payload,omitempty"`
}
