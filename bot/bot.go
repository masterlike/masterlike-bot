package bot

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"masterlike-bot/config"
	"net/http"

	"gopkg.in/mgo.v2/bson"
)

// C is the general instance of DB for bot
var C DB

// Handler is responsible for handling send/receive messages from user.
func Handler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		if r.URL.Path == "/webhook" {
			if r.URL.Query().Get("hub.verify_token") == config.Token {
				fmt.Fprintf(w, "%s", r.URL.Query().Get("hub.challenge"))
				return
			}
			return
		}
	}

	if r.Method == "POST" {
		if r.URL.Path == "/webhook" {
			fmt.Println("Message received")
			d, err := ioutil.ReadAll(r.Body)
			if err != nil {
				fmt.Println("Bad request")
				http.Error(w, fmt.Sprintf("bad request, error %v", err), http.StatusBadRequest)
				return
			}

			log.Printf("%s", string(d))

			fbe := fbMsgEntry{}
			err = json.Unmarshal(d, &fbe)
			if err != nil {
				fmt.Println(err)
			}

			for _, entry := range fbe.Entry {
				fmt.Println("Entry: ", entry)
				for _, message := range entry.Messaging {
					fmt.Println("Message Payload: ", message.Postback.Payload)
					payload := message.Postback.Payload
					if len(payload) > 0 {
						switch {
						case isPoll(payload):
							// Pass information to channel on separate thread to process
							handlePoll(payload, message.Sender.ID)
						default:
							fmt.Println("Not anything")

						}
					}

				}
			}

		}

	}

}

func isPoll(resp string) bool {
	var payload PollPayload

	err := json.Unmarshal([]byte(resp), &payload)
	if err != nil {
		fmt.Println(err.Error())
		return false
	}

	fmt.Println("Payload type: ", payload.Type)

	if payload.Type == PollType {
		return true
	}

	return false
}

func handlePoll(resp string, userID string) {

	fmt.Println("HandlePoll")
	var payload PollPayload
	err := json.Unmarshal([]byte(resp), &payload)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	// For question with questionID
	// 1 - Check if userId has already responded
	n, err := C.Questions.Find(bson.M{"question_id": payload.QuestionID, "responded_ids": userID}).Count()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	if n > 0 {
		return
	}
	// IF 1 true
	// 2 - Update votes for answerID\
	err = C.Questions.Update(bson.M{"question_id": payload.QuestionID, "answers.answer_id": payload.AnswerID},
		bson.M{"$inc": bson.M{"answers.$.votes": 1}, "$addToSet": bson.M{"responded_ids": userID}})
	if err != nil {
		fmt.Println(err)
		return
	}
	// 3 - Add user to already responded list

}
