package schemas

type (

	// Question TODO here
	Question struct {
		ID      int      `bson:"question_id"`
		Text    string   `bson:"text"`
		Answers []Answer `bson:"answers"`
		PageID  string   `bson:"page_id"`
		UserIDs []string `bson:"responded_ids"`
	}

	// Answer TODO here
	Answer struct {
		ID    int    `bson:"answer_id"`
		Text  string `bson:"text"`
		Votes int    `bson:"votes"`
	}
)
