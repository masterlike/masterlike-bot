package main

import (
	"fmt"
	"log"
	"masterlike-bot/bot"
	"net/http"
	"os"

	"gopkg.in/mgo.v2"
)

func main() {

	session, err := mgo.Dial("mongodb://leonardogcsoares:leo-42551625@ds025973.mlab.com:25973/masterlike")
	if err != nil {
		panic(err)
	}

	err = session.Ping()
	if err != nil {
		panic(err)
	}
	defer session.Close()

	questions := session.DB("masterlike").C("questions")

	bot.C = bot.DB{
		Questions: questions,
	}

	// var bot bot.Bot
	fmt.Println("Server Started - 3000")

	http.HandleFunc("/webhook", bot.Handler)
	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), nil))

}
